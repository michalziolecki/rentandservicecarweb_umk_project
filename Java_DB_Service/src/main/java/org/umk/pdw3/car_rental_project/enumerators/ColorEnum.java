package org.umk.pdw3.car_rental_project.enumerators;

public enum ColorEnum {
    RED("red"),
    BLACK("black"),
    BLUE("blue"),
    GREEN("green"),
    WHITE("white"),
    GRAY("gray"),
    PINK("pink"),
    YELLOW("yellow");

    String color;

    ColorEnum(String color){
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
