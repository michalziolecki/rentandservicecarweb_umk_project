package org.umk.pdw3.car_rental_project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.umk.pdw3.car_rental_project.dao.CarTypeDAO;
import org.umk.pdw3.car_rental_project.entity.Car;
import org.umk.pdw3.car_rental_project.entity.CarType;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceCarTypeDAO {

    private CarTypeDAO carType;

    @Autowired
    public ServiceCarTypeDAO(CarTypeDAO carType){
        this.carType = carType;
    }

    public CarType getCarTypeById(int id){
        CarType carType = this.carType.findById(id).get();
        return carType;
    }

    public List<CarType> getCarsType(){
        List<CarType> carTypes = new ArrayList<>();
        this.carType.findAll().forEach(entity -> {
            carTypes.add(entity);
        });
        return carTypes;
    }

    public void saveCarType(CarType carType){
        this.carType.save(carType);
    }

    public void updateCarType(CarType carType){
        this.carType.save(carType);
    }

    public void deleteCarType(CarType carType){
        this.carType.delete(carType);
    }
}
