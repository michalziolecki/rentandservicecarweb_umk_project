package org.umk.pdw3.car_rental_project.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.umk.pdw3.car_rental_project.entity.Car;
import org.umk.pdw3.car_rental_project.entity.CarType;
import org.umk.pdw3.car_rental_project.enumerators.ColorEnum;
import org.umk.pdw3.car_rental_project.service.ServiceCarDAO;
import org.umk.pdw3.car_rental_project.service.ServiceCarTypeDAO;

import java.sql.Date;

@Controller
public class RestControllerCarAndCarType {

    private ServiceCarDAO serviceCarDAO;
    private ServiceCarTypeDAO serviceCarTypeDAO;

    @Autowired
    public RestControllerCarAndCarType(ServiceCarDAO serviceCarDAO, ServiceCarTypeDAO serviceCarTypeDAO){
        this.serviceCarDAO = serviceCarDAO;
        this.serviceCarTypeDAO = serviceCarTypeDAO;
    }

    @RequestMapping(value = {"/car-list"}, method = RequestMethod.GET)
    public String getCarList(Model model){
        model.addAttribute("cars", this.serviceCarDAO.getAllCars());
        return "CarList";
    }

    @RequestMapping(value = {"/insertCar"}, method = RequestMethod.POST)
    public String insertCar(@ModelAttribute Car car,Model model){
        this.serviceCarDAO.saveCar(car);
        model.addAttribute("cars", this.serviceCarDAO.getAllCars());
        return "CarList";
    }

    @RequestMapping(value = {"/add-car"}, method = RequestMethod.GET)
    public String addCar(Model model){
        model.addAttribute("carTypes", this.serviceCarTypeDAO.getCarsType());
        return "AddCar";
    }

    @RequestMapping(value = {"/updateCar"}, method = RequestMethod.GET)
    public String updateCarInDB(@RequestParam int id, Model model){
        Car car= this.serviceCarDAO.getCarById(id);
        model.addAttribute("car", car);
        model.addAttribute("carTypes", this.serviceCarTypeDAO.getCarsType());
        return "UpdateCar";
    }

    @RequestMapping(value = {"/updateExistCar"}, method = RequestMethod.POST)
    public String updateExistCarInDB(@ModelAttribute Car car, Model model){
        this.serviceCarDAO.updateCar(car);
        model.addAttribute("cars", this.serviceCarDAO.getAllCars());
        return "CarList";
    }

    @RequestMapping(value = {"/deleteCar"}, method = RequestMethod.GET)
    public String deleteCarFromDB(@RequestParam Integer id, Model model){
        Car car = this.serviceCarDAO.getCarById(id);
        this.serviceCarDAO.deleteCar(car);
        model.addAttribute("cars", this.serviceCarDAO.getAllCars());
        return "CarList";
    }

    @RequestMapping(value = {"/car-type-list"}, method = RequestMethod.GET)
    public String getCarTypeList(Model model){
        model.addAttribute("carTypes", this.serviceCarTypeDAO.getCarsType());
        return "CarTypeList";
    }

    @RequestMapping(value = {"/add-car-type"}, method = RequestMethod.GET)
    public String addCarType(){
        return "AddCarType";
    }

    @RequestMapping(value = {"/insertCarType"}, method = RequestMethod.POST)
    public String insertCarTypeToDB(@ModelAttribute CarType carType, Model model){
        this.serviceCarTypeDAO.saveCarType(carType);
        model.addAttribute("carTypes", this.serviceCarTypeDAO.getCarsType());
        return "CarTypeList";
    }

    @RequestMapping(value = {"/updateTypeCar"}, method = RequestMethod.GET)
    public String updateCarTypeInDB(@RequestParam int id, Model model){
        CarType carType = this.serviceCarTypeDAO.getCarTypeById(id);
        model.addAttribute("carType", carType);
        return "UpdateCarType";
    }

    @RequestMapping(value = {"/updateExistTypeCar"}, method = RequestMethod.POST)
    public String updateExistCarTypeInDB(@ModelAttribute CarType carType, Model model){
        this.serviceCarTypeDAO.updateCarType(carType);
        model.addAttribute("carTypes", this.serviceCarTypeDAO.getCarsType());
        return "CarTypeList";
    }

    @RequestMapping(value = {"/deleteTypeCar"}, method = RequestMethod.GET)
    public String deleteCarTypeFromDB(@RequestParam("id") Integer carTypeId, Model model){
        CarType carType = this.serviceCarTypeDAO.getCarTypeById(carTypeId);
        this.serviceCarTypeDAO.deleteCarType(carType);
        model.addAttribute("carTypes", this.serviceCarTypeDAO.getCarsType());
        return "CarTypeList";
    }
}
