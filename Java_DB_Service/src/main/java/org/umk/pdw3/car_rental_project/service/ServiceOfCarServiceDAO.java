package org.umk.pdw3.car_rental_project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.umk.pdw3.car_rental_project.dao.CarServiceDAO;
import org.umk.pdw3.car_rental_project.dao.CarTypeDAO;
import org.umk.pdw3.car_rental_project.entity.CarService;
import org.umk.pdw3.car_rental_project.entity.CarType;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceOfCarServiceDAO {

    private CarServiceDAO carServiceDAO;

    @Autowired
    public ServiceOfCarServiceDAO(CarServiceDAO carServiceDAO){
        this.carServiceDAO = carServiceDAO;
    }

    public CarService getCarServiceById(int id){
        CarService carService = this.carServiceDAO.findById(id).get();
        return carService;
    }

    public List<CarService> getCarServices(){
        List<CarService> carServices = new ArrayList<>();
        this.carServiceDAO.findAll().forEach(carService -> {
            carServices.add(carService);
        } );
        return carServices;
    }

    public void saveCarService(CarService carService){
        this.carServiceDAO.save(carService);
    }

    public void updateCarService(CarService carService){
        this.carServiceDAO.save(carService);
    }

    public void deleteCarService(CarService carService){
        this.carServiceDAO.delete(carService);
    }

    public void deleteById(Integer id){
        this.carServiceDAO.deleteById(id);
    }
}
