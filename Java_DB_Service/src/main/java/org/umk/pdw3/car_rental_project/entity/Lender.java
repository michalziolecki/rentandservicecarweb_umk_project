package org.umk.pdw3.car_rental_project.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(indexes = { @Index(name = "IDX_LENDER_id_name_surname", columnList = "id, name, surname") })
public class Lender {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String surname;
    @Column(nullable = false)
    private String personal_ID; //pesel
    @Column(nullable = false)
    private String street;
    @Column(nullable = false)
    private String local;
    @Column(nullable = false)
    private String city;
    @OneToMany(mappedBy = "lender")
    private Set<RentalContract> rentalContracts;

}
