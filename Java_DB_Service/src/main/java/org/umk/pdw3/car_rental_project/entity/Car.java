package org.umk.pdw3.car_rental_project.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.umk.pdw3.car_rental_project.enumerators.ColorEnum;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(indexes = { @Index(name = "IDX_CAR_id_productionYear", columnList = "id, productionYear"),
        @Index(name = "IDX_CAR_id_productionYear", columnList = "id, motor") })
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date productionYear;
    private String descriptionOfCar;
    @Column(nullable = false)
    private Double motor;
    @Column(nullable = false)
    private ColorEnum color;
    @Column(nullable = false)
    private Double costPerDay;
    @ManyToOne
    @JoinColumn( nullable = false, foreignKey = @ForeignKey(value = ConstraintMode.CONSTRAINT, name = "fk_car_car_type_id"))
    private CarType carType;
    @OneToMany(mappedBy = "car")
    private Set<RentalContract> rentalContract;
    @OneToMany(mappedBy = "car")
    private Set<CarService> carServices;

}
