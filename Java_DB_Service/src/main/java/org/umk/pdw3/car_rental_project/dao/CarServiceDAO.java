package org.umk.pdw3.car_rental_project.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.umk.pdw3.car_rental_project.entity.Car;
import org.umk.pdw3.car_rental_project.entity.CarService;

import javax.transaction.Transactional;

@Repository
public interface CarServiceDAO  extends CrudRepository<CarService, Integer> {
    @Override
    @Modifying
    @Transactional
    @Query(value = "delete from car_service where id=:id", nativeQuery = true)
    void deleteById(@Param("id") Integer id);
}
