package org.umk.pdw3.car_rental_project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.pdw3.car_rental_project.dao.CarDAO;
import org.umk.pdw3.car_rental_project.entity.Car;
import org.umk.pdw3.car_rental_project.entity.CarType;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceCarDAO {

    private CarDAO carDAO;

    @Autowired
    public ServiceCarDAO(CarDAO carDAO){
        this.carDAO = carDAO;
    }

    public Car getCarById(int id){
        Car car = this.carDAO.findById(id).get();
        return car;
    }

    public List<Car> getCarsBycostPerDay(Double costPerDay){
        List<Car> carList = this.carDAO.findByCostPerDay(costPerDay);
        return carList;
    }

    public List<Car> getCarsByCarType(CarType carType){
        List<Car> carList = this.carDAO.findByCarType(carType);
        return carList;
    }

    public List<Car> getAllCars(){
        List<Car> carList = new ArrayList<>();
        this.carDAO.findAll().forEach(entity -> {
            carList.add(entity);
        });
        return carList;
    }

    public void saveCar(Car car){
        this.carDAO.save(car);
    }

    public void updateCar(Car car){
        this.carDAO.save(car);
    }

    public void deleteCar(Car car){
        this.carDAO.delete(car);
    }
}
