package org.umk.pdw3.car_rental_project.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.pdw3.car_rental_project.entity.Lender;

import java.util.List;

@Repository
public interface LenderDAO extends CrudRepository<Lender, Long> {
    List<Lender> findBySurname(String surname);
    List<Lender> findByName(String name);
    List<Lender> findByNameAndSurname(String name, String surname);
}
