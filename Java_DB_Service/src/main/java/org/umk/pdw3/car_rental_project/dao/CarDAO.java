package org.umk.pdw3.car_rental_project.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.pdw3.car_rental_project.entity.Car;
import org.umk.pdw3.car_rental_project.entity.CarType;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarDAO extends CrudRepository <Car, Integer> {
    List<Car> findByCarType(CarType carType);
    List<Car> findByCostPerDay(Double costPerDay);
}
