package org.umk.pdw3.car_rental_project.rest;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@Controller
public class RestControllerMainWebsite {

    @RequestMapping(value = {"","/"}, method = RequestMethod.GET)
    public String getMainWebsite(){
        return "index";
    }

}
