package org.umk.pdw3.car_rental_project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.umk.pdw3.car_rental_project.dao.LenderDAO;
import org.umk.pdw3.car_rental_project.dao.RentalContractDAO;
import org.umk.pdw3.car_rental_project.entity.Lender;
import org.umk.pdw3.car_rental_project.entity.RentalContract;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceRentalConctractDAO {

    private RentalContractDAO rentalContractDAO;

    @Autowired
    public ServiceRentalConctractDAO(RentalContractDAO rentalContractDAO){
        this.rentalContractDAO = rentalContractDAO;
    }

    public List<RentalContract> getAllRentalContract(){
        List<RentalContract> rentalContractList = new ArrayList<>();
        this.rentalContractDAO.findAll().forEach(contract ->{
            rentalContractList.add(contract);
        });
        return rentalContractList;
    }

    public RentalContract getRentalContractById(long id){
        RentalContract rentalContract =  this.rentalContractDAO.findById(id).get();
        return rentalContract;
    }

    public List<RentalContract> getRentalContractByRentFrom(Timestamp rentFrom){
        List<RentalContract> rentalContracts = this.rentalContractDAO.findByRentFrom(rentFrom);
        return rentalContracts;
    }

    public List<RentalContract> getRentalContractByRentTo(Timestamp rentTo){
        List<RentalContract> rentalContracts = this.rentalContractDAO.findByRentTo(rentTo);
        return rentalContracts;
    }

    public List<RentalContract> getRentalContractByRentFromTo(Timestamp rentFrom, Timestamp rentTo){
        List<RentalContract> rentalContracts = this.rentalContractDAO.findByRentFromAndRentTo(rentFrom, rentTo);
        return rentalContracts;
    }

    public List<RentalContract> getByLenders(Lender lender){
        List<RentalContract> rentalContracts = this.rentalContractDAO.findByLender(lender);
        return rentalContracts;
    }

    public void saveRentalContract(RentalContract rentalContract){
        this.rentalContractDAO.save(rentalContract);
    }

    public void updateRentalContract(RentalContract rentalContract){
        this.rentalContractDAO.save(rentalContract);
    }

    public void deleteRentalContract(RentalContract rentalContract){
        this.rentalContractDAO.delete(rentalContract);
    }

    public void deleteById(Long id){
        this.rentalContractDAO.deleteById(id);
    }

}
