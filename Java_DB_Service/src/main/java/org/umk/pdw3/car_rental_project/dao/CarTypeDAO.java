package org.umk.pdw3.car_rental_project.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.umk.pdw3.car_rental_project.entity.CarType;

import javax.transaction.Transactional;

@Repository
public interface CarTypeDAO extends CrudRepository<CarType, Integer> {
}
