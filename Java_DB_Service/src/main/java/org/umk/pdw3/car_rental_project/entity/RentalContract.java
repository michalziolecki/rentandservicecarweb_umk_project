package org.umk.pdw3.car_rental_project.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(indexes = { @Index(name = "IDX_CONTRACT_id_car_lender", columnList = "id, car_id, lender_id") ,
        @Index(name = "IDX_LENDER_id_rentFrom_rentTo", columnList = "id, rentFrom, rentTo") })
public class RentalContract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date rentFrom;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date rentTo;
    @Column(nullable = false)
    private Double costOfRent;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(nullable = false,
            foreignKey = @ForeignKey(value = ConstraintMode.CONSTRAINT, name = "fk_rental_contract_car_id"))
    private Car car;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(nullable = false,
            foreignKey = @ForeignKey(value = ConstraintMode.CONSTRAINT, name = "fk_rental_contract_lender_id"))
    private Lender lender;

}
