package org.umk.pdw3.car_rental_project.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.umk.pdw3.car_rental_project.entity.Car;
import org.umk.pdw3.car_rental_project.entity.Lender;
import org.umk.pdw3.car_rental_project.entity.RentalContract;
import org.umk.pdw3.car_rental_project.service.ServiceCarDAO;
import org.umk.pdw3.car_rental_project.service.ServiceLenderDAO;
import org.umk.pdw3.car_rental_project.service.ServiceRentalConctractDAO;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
public class RestControllerRentalContract {

    private ServiceRentalConctractDAO serviceRentalConctractDAO;
    private ServiceCarDAO serviceCarDAO;
    private ServiceLenderDAO serviceLenderDAO;


    @Autowired
    public RestControllerRentalContract(ServiceRentalConctractDAO serviceRentalConctractDAO,
                                        ServiceCarDAO serviceCarDAO, ServiceLenderDAO serviceLenderDAO){
        this.serviceRentalConctractDAO = serviceRentalConctractDAO;
        this.serviceCarDAO = serviceCarDAO;
        this.serviceLenderDAO = serviceLenderDAO;
    }

    @RequestMapping(value = {"/rent-car"}, method = RequestMethod.GET)
    public String rentCar(Model model){
        List<Car> allCars = this.serviceCarDAO.getAllCars();
        List<Lender> allLenders = this.serviceLenderDAO.getAllLenders();
        model.addAttribute("cars",allCars);
        model.addAttribute("lenders",allLenders);
        return "RentCar";
    }

    @RequestMapping(value = {"/insert-rental-contract"}, method = RequestMethod.POST)
    public String insertRentalContract(@ModelAttribute RentalContract rentalContract, Model model){
        long diffInMillies = Math.abs(rentalContract.getRentTo().getTime() - rentalContract.getRentFrom().getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        Double costOfRent = rentalContract.getCar().getCostPerDay() * (diff+1.0);
        rentalContract.setCostOfRent(costOfRent);

        this.serviceRentalConctractDAO.saveRentalContract(rentalContract);
        model.addAttribute("rentcars", this.serviceRentalConctractDAO.getAllRentalContract());
        return "RentCarList";
    }

    @RequestMapping(value = {"/list-rented-car"}, method = RequestMethod.GET)
    public String rentCarList(Model model){
        model.addAttribute("rentcars", this.serviceRentalConctractDAO.getAllRentalContract());
        return "RentCarList";
    }

    @RequestMapping(value = {"/updateRentedCar"}, method = RequestMethod.GET)
    public String updateRentCar(@RequestParam("id") Integer id, Model model){
        RentalContract rentedCar = this.serviceRentalConctractDAO.getRentalContractById(id);
        model.addAttribute("rentedcar", rentedCar);
        model.addAttribute("cars",this.serviceCarDAO.getAllCars());
        model.addAttribute("lenders",this.serviceLenderDAO.getAllLenders());
        return "UpdateRentedCar";
    }

    @RequestMapping(value = {"/updateExistRentedCar"}, method = RequestMethod.POST)
    public String updateRentCar(@ModelAttribute RentalContract rentalContract, Model model){

        Car car = this.serviceCarDAO.getCarById(rentalContract.getCar().getId());
        rentalContract.setCar(car);
        Lender lender = this.serviceLenderDAO.getLenderById(rentalContract.getLender().getId());
        rentalContract.setLender(lender);

        long diffInMillies = Math.abs(rentalContract.getRentTo().getTime() - rentalContract.getRentFrom().getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        Double costOfRent = rentalContract.getCar().getCostPerDay() * (diff+1.0);
        rentalContract.setCostOfRent(costOfRent);

        this.serviceRentalConctractDAO.updateRentalContract(rentalContract);
        model.addAttribute("rentcars", this.serviceRentalConctractDAO.getAllRentalContract());
        return "RentCarList";
    }

    @RequestMapping(value = {"/deleteRentedCar"}, method = RequestMethod.GET)
    public String deleteRentedCar(@RequestParam("id") long id, Model model){
        this.serviceRentalConctractDAO.deleteById(id);
        model.addAttribute("rentcars", this.serviceRentalConctractDAO.getAllRentalContract());
        return "RentCarList";
    }
}
