package org.umk.pdw3.car_rental_project.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.umk.pdw3.car_rental_project.entity.Lender;
import org.umk.pdw3.car_rental_project.service.ServiceLenderDAO;

@Controller
public class RestControllerLender {

    private ServiceLenderDAO serviceLenderDAO;

    public RestControllerLender(ServiceLenderDAO serviceLenderDAO){
        this.serviceLenderDAO = serviceLenderDAO;
    }

    @RequestMapping(value = {"/registry-lender"}, method = RequestMethod.GET)
    public String registerLender(){
        return "RegisterLender";
    }

    @RequestMapping(value = {"/lender-list"}, method = RequestMethod.GET)
    public String getLenderList(Model model){
        model.addAttribute("lenders", this.serviceLenderDAO.getAllLenders());
        return "LenderList";
    }

    @RequestMapping(value = {"/insertLender"}, method = RequestMethod.POST)
    public String insertLender(@ModelAttribute Lender lender, Model model){
        this.serviceLenderDAO.saveLender(lender);
        model.addAttribute("lenders", this.serviceLenderDAO.getAllLenders());
        return "LenderList";
    }

    @RequestMapping(value = {"/updateLender"}, method = RequestMethod.GET)
    public String updateLender(@RequestParam long id , Model model){
        Lender lender = this.serviceLenderDAO.getLenderById(id);
        model.addAttribute("lender", lender);
        return "UpdateLender";
    }

    @RequestMapping(value = {"/updateExistLender"}, method = RequestMethod.POST)
    public String updateExistLender(@ModelAttribute Lender lender, Model model){
        this.serviceLenderDAO.updateLender(lender);
        model.addAttribute("lenders", this.serviceLenderDAO.getAllLenders());
        return "LenderList";
    }

    @RequestMapping(value = {"/deleteLender"}, method = RequestMethod.GET)
    public String deleteLender(@RequestParam long id, Model model){
        Lender lender = this.serviceLenderDAO.getLenderById(id);
        this.serviceLenderDAO.deleteLender(lender);
        model.addAttribute("lenders", this.serviceLenderDAO.getAllLenders());
        return "LenderList";
    }
}
