package org.umk.pdw3.car_rental_project.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.umk.pdw3.car_rental_project.entity.Car;
import org.umk.pdw3.car_rental_project.entity.CarService;
import org.umk.pdw3.car_rental_project.service.ServiceCarDAO;
import org.umk.pdw3.car_rental_project.service.ServiceOfCarServiceDAO;

import javax.validation.Valid;

@Controller
public class RestControllerCarService {

    private ServiceOfCarServiceDAO serviceOfCarServiceDAO;
    private ServiceCarDAO serviceCarDAO;


    @Autowired
    public RestControllerCarService(ServiceOfCarServiceDAO serviceOfCarServiceDAO, ServiceCarDAO serviceCarDAO){
        this.serviceOfCarServiceDAO = serviceOfCarServiceDAO;
        this.serviceCarDAO = serviceCarDAO;
    }

    @RequestMapping(value = {"/add-car-to-service"}, method = RequestMethod.GET)
    public String addCarToService(Model model){
        model.addAttribute("cars", this.serviceCarDAO.getAllCars());
        return "AddCarToService";
    }

    @RequestMapping(value = {"/insert-car-to-service"}, method = RequestMethod.POST)
    public String insertCarToService(@ModelAttribute CarService carService, Model model, BindingResult result){
//        System.out.println("test" + result.getModel().toString());
        this.serviceOfCarServiceDAO.saveCarService(carService);
        model.addAttribute("servicedcars", this.serviceOfCarServiceDAO.getCarServices());
        return "ServicesCarList";
    }

    @RequestMapping(value = {"/list-services-car"}, method = RequestMethod.GET)
    public String getListServicesCar(Model model){
        model.addAttribute("servicedcars", this.serviceOfCarServiceDAO.getCarServices());
        return "ServicesCarList";
    }

    @RequestMapping(value = {"/deleteServicedCar"}, method = RequestMethod.GET)
    public String deleteServicedCar(@RequestParam("id") Integer id, Model model){
        this.serviceOfCarServiceDAO.deleteById(id);
        model.addAttribute("servicedcars", this.serviceOfCarServiceDAO.getCarServices());
        return "ServicesCarList";
    }

    @RequestMapping(value = {"/updateServicedCar"}, method = RequestMethod.GET)
    public String updateServicedCar(@RequestParam("id") Integer id, Model model){
        CarService servicedCar = this.serviceOfCarServiceDAO.getCarServiceById(id);
//        servicedCar.setRepairedFrom;
        model.addAttribute("cars", this.serviceCarDAO.getAllCars());
        model.addAttribute("servicedcar", servicedCar);
        return "UpdateServicedCar";
    }

    @RequestMapping(value = {"/updateExistServicedCar"}, method = RequestMethod.POST)
    public String updateExistServicedCar(@ModelAttribute CarService carServiced, Model model){
        Car car = this.serviceCarDAO.getCarById(carServiced.getCar().getId());
        carServiced.setCar(car);
        this.serviceOfCarServiceDAO.updateCarService(carServiced);
        model.addAttribute("servicedcars", this.serviceOfCarServiceDAO.getCarServices());
        return "ServicesCarList";
    }

}
