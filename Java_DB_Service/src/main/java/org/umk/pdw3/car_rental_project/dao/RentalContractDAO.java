package org.umk.pdw3.car_rental_project.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.umk.pdw3.car_rental_project.entity.Lender;
import org.umk.pdw3.car_rental_project.entity.RentalContract;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface RentalContractDAO extends CrudRepository<RentalContract, Long> {
    List<RentalContract> findByRentFrom(Timestamp rentFrom);
    List<RentalContract> findByRentTo(Timestamp rentTo);
    List<RentalContract> findByRentFromAndRentTo(Timestamp rentFrom, Timestamp rentTo);
    List<RentalContract> findByLender(Lender lender);

    @Override
    @Modifying
    @Transactional
    @Query(value = "delete from rental_contract where id=:id", nativeQuery = true)
    void deleteById(@Param("id") Long id);
}
