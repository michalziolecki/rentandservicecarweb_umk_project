package org.umk.pdw3.car_rental_project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.umk.pdw3.car_rental_project.dao.LenderDAO;
import org.umk.pdw3.car_rental_project.entity.Lender;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceLenderDAO {

    private LenderDAO lenderDAO;

    @Autowired
    public ServiceLenderDAO(LenderDAO lenderDAO){
        this.lenderDAO = lenderDAO;
    }

    public Lender getLenderById(long id){
        Lender lender = this.lenderDAO.findById(id).get();
        return lender;
    }

    public List<Lender> getLendersBySurname(String surname){
        List<Lender> lenders = this.lenderDAO.findBySurname(surname);
        return lenders;
    }

    public List<Lender> getLendersByName(String name){
        List<Lender> lenders = this.lenderDAO.findByName(name);
        return lenders;
    }

    public List<Lender> getLendersByNameAndSurname(String name, String surname){
        List<Lender> lenders = this.lenderDAO.findByNameAndSurname(name,surname);
        return null;
    }

    public List<Lender> getAllLenders(){
        List<Lender> lenders = new ArrayList<>();
        this.lenderDAO.findAll().forEach(entity -> {
            lenders.add(entity);
        });
        return lenders;
    }

    public void saveLender(Lender lender){
        this.lenderDAO.save(lender);
    }

    public void updateLender(Lender lender){
        this.lenderDAO.save(lender);
    }

    public void deleteLender(Lender lender){
        this.lenderDAO.delete(lender);
    }
}
