package org.umk.pdw3.car_rental_project.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(indexes = { @Index(name = "IDX_CARTYPE_id_mark", columnList = "id, mark") })
public class CarType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true)
    private String mark;
    @Column(nullable = false, unique = true)
    private String typeOfCar;
    @OneToMany(mappedBy = "carType", cascade = CascadeType.ALL)
    private Set<Car> cars;

    @Override
    public String toString() {
        return "CarType{" +
                "id=" + id +
                ", mark='" + mark + '\'' +
                ", typeOfCar='" + typeOfCar + '\'' +
                ", cars=" + cars +
                '}';
    }
}
