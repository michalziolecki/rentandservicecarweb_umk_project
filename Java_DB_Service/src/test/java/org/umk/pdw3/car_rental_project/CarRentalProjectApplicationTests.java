package org.umk.pdw3.car_rental_project;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CarRentalProjectApplicationTests {

    @Test
    public void contextLoads() {
    }

}
