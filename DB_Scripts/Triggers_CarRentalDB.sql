DELIMITER $$ 

    CREATE TRIGGER before_car_service_insert_status BEFORE INSERT ON car_service
    FOR EACH ROW BEGIN
      IF NEW.repaired_to IS NULL THEN
            SET NEW.repair_status = 0;
      ELSE
            SET NEW.repair_status = 1;
      END IF;
    END$$

DELIMITER ;

DELIMITER $$ 

    CREATE TRIGGER before_car_service_update_status  BEFORE UPDATE ON car_service
    FOR EACH ROW BEGIN
      IF NEW.repaired_to IS NULL  THEN
            SET NEW.repair_status = 0;
      ELSE
            SET NEW.repair_status = 1;
      END IF;
    END$$

DELIMITER ;