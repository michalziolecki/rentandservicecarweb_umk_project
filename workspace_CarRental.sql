create database CarRental;

use CarRental;

show tables;
-- show indexes ;

select * from car;
describe car;
describe car_type;
describe car_service;
describe lender;
describe rental_contract;


drop table lender;
drop table car;
drop table car_type;
drop table car_service;
drop table rental_contract;
drop database CarRental;

select * from car;
select * from rental_contract;
select * from car_type;
select * from car_service;

delete from rental_contract where id=3;

insert into car (color, cost_per_day, description, motor, production_year, car_type_id)
 values(1,120,'opis3',1.2,'1992-10-10',2);
insert into car_type (mark, type) values('mark2', 'type2');
insert into car_service (problem_description, repaired_from, car_id) 
values('problem10', '2018-10-10', 2);
insert into car_service (problem_description, repaired_from, repaired_to, car_id) 
values('problem10', '2018-10-10', '2018-10-11', 2);


update car_service c set repair_status = 0 where c.id = 6;
update car_service c set repaired_from = '2018-10-11' where c.id = 9;

create trigger before_car_service_insert_status 
before insert on car_service
for each row set new.repair_status = 0;

create trigger before_car_service_update_status 
before update on car_service
for each row set new.repair_status = 1;

DELIMITER $$ 

    CREATE TRIGGER before_car_service_insert_status BEFORE INSERT ON car_service
    FOR EACH ROW BEGIN
      IF NEW.repaired_to IS NULL THEN
            SET NEW.repair_status = 0;
      ELSE
            SET NEW.repair_status = 1;
      END IF;
    END$$

DELIMITER ;

DELIMITER $$ 

    CREATE TRIGGER before_car_service_update_status  BEFORE UPDATE ON car_service
    FOR EACH ROW BEGIN
      IF NEW.repaired_to IS NULL  THEN
            SET NEW.repair_status = 0;
      ELSE
            SET NEW.repair_status = 1;
      END IF;
    END$$

DELIMITER ;

show triggers;
drop trigger before_car_service_insert_status;

